#!/bin/bash

# script for uploading gold image to vcenter
# Future: add --force option to force uploading template even if it already exists

# get datacenter for later operations
DC=$(govc find -type d)
VM_FOLDER="${DC}/vm/test"
OVF_PATH="/home/brad/remote/ovf"
DATASTORE=$(govc find -type s -name '*' | head -n 1)

# check for VM folder and create if it doesn't exist
if [ $(govc find -type f -name 'test') = $VM_FOLDER ]
  then
    echo "folder exists"
  else
    echo "creating vm folder"
    govc folder.create "$VM_FOLDER"
fi

# install jq if necessary to parse json
# check if template exists and is latest version, but how do we know what the latest version is? compare packer timestamp to ovf timestamp ish

if [ $(govc find -type m -name el7_packer) ]
  then
    echo "template exists"
  else
    echo "template not found, uploading"
    govc import.ovf -name el7_packer -datastore "$DATASTORE" -folder "$VM_FOLDER" ${OVF_PATH}/el7_packer.ovf
fi

#govc vm.info -json "el7_packer" | jq .VirtualMachines[].Config.Annotation

# if it does exist check whether timestamp in the notes is >= first of the month

# if older then delete template and run packer
# else exit cleanly "template already up to date"